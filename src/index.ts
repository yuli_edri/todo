import log from "@ajar/marker";
import fs from 'fs/promises';

import myFunctions from "./myModule.js"; 

let functions =new myFunctions();

async function init(){
    try{
         await fs.readFile('./todo.json', 'utf8');
     } catch (err) {
      await fs.writeFile('./todo.json', JSON.stringify([],null,2));
     }
     
       let myArgs = process.argv.slice(2);
        switch (myArgs[0]) {
            case 'create':  
               log.obj(await functions.create(myArgs[1]));
               break;
    
            case 'read':
                log.obj( await functions.read());
                break;
                
            case 'update':
                log.obj(await functions.Update(myArgs[1]));
                break;
                
            case 'delete_task':
             log.obj(await functions.deleting(myArgs[1]));
                break;
            
                
            }
}

init();
