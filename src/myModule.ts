
import fs from 'fs/promises';

export  default class myFunctions{
     async  create(str:string):Promise<string[]>{
        const data_json = await fs.readFile('./todo.json', 'utf8');
        const data = JSON.parse(data_json);
        //console.log(data);
        data.push({name :  str,check : false,id: Date.now()})
        await fs.writeFile('./todo.json', JSON.stringify(data,null,2));
        return data;
    
    }
     async  read():Promise<string[]>{
        const data_json = await fs.readFile('./todo.json', 'utf8');
        const data = JSON.parse(data_json);
        return  data;
    }
    
     async  Update(id:string):Promise<string[]> {
        const data_json = await fs.readFile('./todo.json', 'utf8');
        //console.log('hiii')
        const data = JSON.parse(data_json);
        for(let task of data){
           // console.log(task.id.toString() === id);
            if(task.id.toString()=== id){
                //console.log(task.id);
                task.check =!task.check;
                await fs.writeFile('./todo.json', JSON.stringify(data,null,2));
             
               
            }
        }
        return data;
    }
     async deleting(id:string){
        let data_json = await fs.readFile('./todo.json', 'utf8');
        let data = JSON.parse(data_json) as {name:string,check:boolean,id:number}[];
        data = data.filter((task) =>  task.id.toString()!==id);
        await fs.writeFile('./todo.json', JSON.stringify(data,null,2));
        return data;
        
    }
}

